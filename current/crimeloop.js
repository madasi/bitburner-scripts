//Madasi - v2.0.0
export class Crime {
    constructor(name, profit) {
        this.name = name;
        this.profit = profit;
    }
}

let crimes = [new Crime("shoplift", 15/2),
              new Crime("rob store", 400/60),
              new Crime("mug",  36/4),
              new Crime("larceny", 800/90),
              new Crime("deal drugs", 120/10),
              new Crime("bond forgery", 4500/300),
              new Crime("traffick arms", 600/40),
              new Crime("homicide", 45/3),
              new Crime("grand theft auto", 1600/80),
              new Crime("kidnap", 3600/120),
              new Crime("assassinate", 12000/300),
              new Crime("heist", 120000/600)
];

function tryCrime(ns) {
    let bestCrime = crimes[0];
    let bestScore = ns.getCrimeChance(bestCrime.name) * bestCrime.profit;
    for (var i = 1; i < crimes.length; i++) {
        let crime = crimes[i];
        let score = ns.getCrimeChance(crime.name) * crime.profit;
        if (score > bestScore) {
            bestCrime = crime;
            bestScore = score;
        }
    }
    return ns.commitCrime(bestCrime.name);
}

export async function main(ns) {
    while(true) {
        ns.stopAction();
        let actionTime = tryCrime(ns);
        if (actionTime > 0) {
            await ns.sleep(actionTime - 1000);
            if (!ns.isBusy()) {
                await ns.sleep(10000);
            }
            while(ns.isBusy()) {
                await ns.sleep(500);
            }
        }
        await ns.sleep(500);
    }
}
