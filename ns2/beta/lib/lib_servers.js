// lib_servers.js - 1.85GB
// Madasi v2.0.1

// returns an array of all servers in the game
/*export const array_get_servers = (ns) => {
  const array_servers = [ns.getHostname()];
  array_servers.forEach(string_server => {
    ns.scan(string_server).forEach(string_scan_result => {
      -1 === array_servers.indexOf(string_scan_result) &&
        array_servers.push(string_scan_result);
    });
  });
  return array_servers;
};*/

export const array_get_servers = function(ns) {
    ns.disableLog("disableLog");
    ns.disableLog("enableLog");
    ns.disableLog("scan");
    const array_servers = [];
    const work_queue = [ns.getHostname()];
    while(work_queue.length > 0){
        //ns.tprint("array_servers:\n\n" + array_servers + "\n\nwork_queue:\n\n" + work_queue);
        var  current_server=work_queue.shift();
        if(-1 === array_servers.indexOf(current_server)){
            array_servers.push(current_server);
            var scan_result = ns.scan(current_server);
            for(let i=0;i<scan_result.length;i++){
                if(-1 === work_queue.indexOf(scan_result[i]) && -1 === array_servers.indexOf(scan_result[i])){
                    work_queue.push(scan_result[i]);
                }
            }
        }
        //await ns.sleep(100);
    }
    //ns.tprint("Returning:\n\n" + array_servers);
    ns.enableLog("scan");
    ns.enableLog("disableLog");
    ns.enableLog("enableLog");
    return array_servers;
};