// Original from a script By RafnarC - https://www.reddit.com/r/Bitburner/comments/75thik/reputation_script/
// Heavily modified, customised, and later converted to NS2 by Madasi
// v2.1.8-debug 
//TODO: buy other programs

// ARRRGs and config
// current amount of job rep to earn
var corprep = 10000;
// max amount of job rep to earn before switching to a new job
var maxcorprep = 250000;
// how long to sleep for normal activities
var sleeptime =240000;
// percentage of income to donate for favor from a faction per attempt
var percentToDonate = 0.02;
// how much to increase the rep targets per loop cycle
var incrementRep = 10000;
// how long to train each stat for per loop
var statSleep = sleeptime/2;
// how high to train stats (except charisma which is currently hardcoded to 1000)
var statTarget = 1500;
// how much money is needed to train stats = seconds training * cost/sec + 2 tickets cost * 2 (safety buffer)
var enoughMoney=((((statSleep/1000)*2400)+400000)*2);
// filename for tracking last world tour timestamp
var tourTime = "worldTour.txt";

//preset data structures
//list of all corp
var corps = ["MegaCorp", "Blade Industries", "Four Sigma", "KuaiGong International", "NWO", "OmniTek Incorporated", "ECorp", "Bachman & Associates", "Clarke Incorporated", "Fulcrum Technologies"];
//list of crimes and payouts
var crimes = [["shoplift", 15/2],
["rob store", 400/60],
["mug",  36/4],
["larceny", 800/90],
["deal drugs", 120/10],
["bond forgery", 4500/300],
["traffick arms", 600/40],
["homicide", 45/3],
["grand theft auto", 1600/80],
["kidnap", 3600/120],
["assassinate", 12000/300],
["heist", (120000/600)*0.9] //penalty b/c it is always chosen too early
];
//list of known cities
var cities =["Aevum", "Chongqing", "Sector-12", "New Tokyo", "Ishima", "Volhaven"];


export class Faction {
    constructor(name, factionRep, topAug, culled) {
        this.name = name;
        this.factionRep = factionRep;
        this.topAug = topAug;
        this.culled = culled;
    }
}

async function work(ns, faction){
    while (ns.getFactionRep(faction.name) < faction.factionRep) {
        ns.print("Current Faction Rep: " + ns.getFactionRep(faction.name) + " is < rep setting: " + faction.factionRep);
        ns.donateToFaction(faction.name, ns.getServerMoneyAvailable("home") * percentToDonate);
        if(ns.getFactionRep(faction.name) >= faction.factionRep){
            continue;
        }
        if(!ns.workForFaction(faction.name, "hacking")){
            ns.workForFaction(faction.name,"fieldwork");
        }
        await ns.sleep(sleeptime);
        ns.stopAction();
    }
}

async function workjob(ns, company){
    if(company!="Joe's Guns"){
        ns.applyToCompany(company,"software");
    } else{
        ns.applyToCompany(company,"employee");
    }
    ns.print("company: " + company + " corprep: " + corprep + " current rep: " + ns.getCompanyRep(company));
    while (ns.getCompanyRep(company) < corprep) {
        ns.print("Current Company Rep: " + ns.getCompanyRep(company) + " is < corprep setting: " + corprep);
        ns.workForCompany(company);
        await ns.sleep(sleeptime);
        ns.stopAction();
    }
}

async function tryCrime(ns, target) {
    ns.disableLog("sleep");
    //ns.disableLog("applyToCompany");
    ns.print("Committing crimes to earn at least " + target);
    while(ns.getServerMoneyAvailable("home") < target){
        //ns.applyToCompany("Joe's Guns","employee");
        //ns.workForCompany("Joe's Guns");
        let bestCrime = crimes[0];
        let bestScore = ns.getCrimeChance(bestCrime[0]) * bestCrime[1];
        for (var i = 1; i < crimes.length; i++) {
            let crime = crimes[i];
            let score = ns.getCrimeChance(crime[0]) * crime[1];
            if (score > bestScore) {
                bestCrime = crime;
                bestScore = score;
            }
        }
        let actionTime = ns.commitCrime(bestCrime[0]);
        if (actionTime > 0) {
            await ns.sleep(actionTime - 1000);
            while(ns.isBusy()) {
                await ns.sleep(500);
            }
        }
        await ns.sleep(100);
    }
    ns.enableLog("sleep");
    //ns.enableLog("applyToCompany");
}

function idle(ns) {
    ns.print("Idling (at work)");
    //disableLog("applyToCompany");
    //applyToCompany("Joe's Guns","employee");
    //workForCompany("Joe's Guns");
    //enableLog("applyToCompany");
    ns.workForCompany();
}

export async function main(ns){
    // ARRRGs and config
    // current amount of faction rep to earn < deprecated for per faction calculations
    //var rep = 5000;
    // current amount of job rep to earn
    corprep = 10000;
    // track which job we are currently earning rep for
    var corpindex = 0;
    // how much money to have before training stats
    var moneyTarget = 4000000;
    // flag for if we've visited all the cities for potential faction invites yet (untested)
    var hasToured = false;
    // do we have a TOR router yet?
    var hasTor = false;
    // track how many hacking programs we own
    var numPrograms = 0;
    // creating the list of faction you're already in 
    var currentfactions =[];
    // creating the list of factions we've already earned the max augment rep cost for
    //var cullfactions = [];
    // Now that I'm using classes I need two normal arrays back, but can skip cullfactions :(
    var seenFactions = [];
    var myFactions = [];
    
    ns.disableLog("disableLog");
    ns.disableLog("enableLog");
    ns.disableLog("getServerMoneyAvailable");
    //Check when we last took a world tour (to avoid on on every reload or script launch)
    let lastTour = Number(ns.read(tourTime));
    if(!hasToured && lastTour != 0){
        if(ns.getTimeSinceLastAug() > lastTour) {
            //We haven't reset since the last tour, no need to tour again
            hasToured = true;
            ns.print("I see we've toured the world recently. No need to do that again then.");
        } else {
            //Looks like we've reset since the last tour
            ns.clear(tourTime);
            ns.print("You've changed since your last world tour. I'll schedule you a new one.");
        }
    }
    //starting the loop
    while (true){
        // check faction invites and accept them then remove them from the knowfacttions list 
        ns.print("top of loop, checking for faction invites");
        let invites = ns.checkFactionInvitations();
        if (invites.length > 0 ){
            ns.print("Found " + invites.length + " invites");
            //rep = Math.max(5000,rep/(invites.length + 1));
            for (let i=0; i<invites.length;i++){
                ns.print("Accepting invite: " + invites[i]);
                ns.joinFaction(invites[i]);
            }
        }
        
        
        var factionaugs= [];
        ns.print("Retrieving current faction membership");
        currentfactions = ns.getCharacterInformation().factions;
        for (let i=0; i<currentfactions.length; i++){
            if(!seenFactions.includes(currentfactions[i])){
                seenFactions.push(currentfactions[i]);
                factionaugs = ns.getAugmentationsFromFaction(currentfactions[i]);
                var topaug=1;
                for(let n=0;n<factionaugs.length;n++){
                    if (factionaugs[n]!="NeuroFlux Governor"){
                        topaug=Math.max(ns.getAugmentationCost(factionaugs[n])[0], topaug);
                    }
                }
                let thisFaction = new Faction(currentfactions[i], 5000, topaug, false);
                myFactions.push(thisFaction);
            }
        }
        
        //Cull faction you can already buy all augments from (excluding "NeuroFlux Governor")
        
        
        ns.print("Culling factions you already have all augments from");
        for(let i=0;i<myFactions.length;i++){
            if(myFactions[i].culled){
                continue;
            }
            ns.print("Checking: " + myFactions[i].name);
            if (ns.getFactionRep(myFactions[i].name)>myFactions[i].topAug){
                myFactions[i].culled = true;
                ns.print("Culling: " + myFactions[i].name + " because you have more than " + myFactions[i].topAug + " reputation already");
            }
        }
        
        //Let's get a minimum amount of stats
        ns.print("Checking for a minimum amount of stats");
        while(ns.getStats().hacking < 10 && ns.getCharacterInformation().city == "Sector-12") {
            ns.universityCourse("Rothman University", "Study Computer Science");
            await ns.sleep(60000);
        }
        if(ns.getStats().strength < 10)
        {
        ns.applyToCompany("Joe's Guns","employee");
        ns.workForCompany("Joe's Guns");
	    await ns.sleep(120000);
        }
        if(ns.getStats().defense < 10)
        {
        ns.applyToCompany("Joe's Guns","employee");
        ns.workForCompany("Joe's Guns");
	await ns.sleep(120000);
        }
        if(ns.getStats().dexterity < 10)
        {
        ns.applyToCompany("Joe's Guns","employee");
        ns.workForCompany("Joe's Guns");
	await ns.sleep(120000);
        }
        if(ns.getStats().agility < 10)
        {
        ns.applyToCompany("Joe's Guns","employee");
        ns.workForCompany("Joe's Guns");
	await ns.sleep(120000);
        }
        if(ns.getStats().charisma < 10)
        {
        ns.applyToCompany("Joe's Guns","employee");
        ns.workForCompany("Joe's Guns");
	await ns.sleep(120000);
        }
        
        //Commit crimes if low on money
        if(ns.getServerMoneyAvailable("home") < moneyTarget) {
            await tryCrime(ns, moneyTarget);
        }
        
        //Check if you're in a faction to get rep from 
        ns.print("Checking whether to work a faction for reputation. myFactions.length: " + myFactions.length);
        if(myFactions.length>0){
            // work for faction
            for (let i=0;i<currentfactions.length;i++){
                if(myFactions[i].culled){
                    continue;
                }
                ns.print("Working for " + myFactions[i].name + " current rep setting: " + myFactions[i].factionRep);
                await work(ns, myFactions[i]);
                myFactions[i].factionRep = ns.getFactionRep(myFactions[i].name) + incrementRep;
            }
        }
        //work job
        ns.print("Checking whether to work a job");
        if(ns.getHackingLevel()>250){
            if (corpindex < corps.length){
                ns.print("Working for " + corps[corpindex] + " current rep setting: " + corprep + " current rep: " + ns.getCompanyRep(corps[corpindex]));
                await workjob(ns, corps[corpindex]);
                corprep = ns.getCompanyRep(corps[corpindex]) + incrementRep;
                // switching to a new corp if your rep is above 250000 or the set maxcorprep
                if (ns.getCompanyRep(corps[corpindex]) > maxcorprep ){
                    ns.print("I quit! Moving on to the next job");
                    corpindex++;
                    corprep = 10000;
                }
            }
        } else {
            await workjob(ns, "Joe's Guns");
        }
        
        var myStats=ns.getStats();
        var myCity=ns.getCharacterInformation().city;
        
        if(myStats.strength < statTarget && ns.getServerMoneyAvailable("home") > enoughMoney) {
            ns.print("Getting swole");
            if( ns.getCharacterInformation().city != "Sector-12"){
                ns.travelToCity("Sector-12");
            }
            if(ns.gymWorkout("Powerhouse Gym", "strength")) {
                await ns.sleep(statSleep);
            }
        }
        if(myStats.defense < statTarget && ns.getServerMoneyAvailable("home") > enoughMoney) {
            ns.print("Improving defense");
            if( ns.getCharacterInformation().city != "Sector-12"){
                ns.travelToCity("Sector-12");
            }
            if(ns.gymWorkout("Powerhouse Gym", "defense")) {
                await ns.sleep(statSleep);
            }
        }
        if(myStats.dexterity < statTarget && ns.getServerMoneyAvailable("home") > enoughMoney) {
            ns.print("Getting dextrous");
            if( ns.getCharacterInformation().city != "Sector-12"){
                ns.travelToCity("Sector-12");
            }
            if(ns.gymWorkout("Powerhouse Gym", "dexterity")) {
                await ns.sleep(statSleep);
            }
        }
        if(myStats.agility < statTarget && ns.getServerMoneyAvailable("home") > enoughMoney) {
            ns.print("Getting agile");
            if( ns.getCharacterInformation().city != "Sector-12"){
                ns.travelToCity("Sector-12");
            }
            if(ns.gymWorkout("Powerhouse Gym", "agility")) {
                await ns.sleep(statSleep);
            }
        }
        if(myStats.charisma < 1000 && ns.getServerMoneyAvailable("home") > enoughMoney) {
            ns.print("Learning to be more charismatic");
            if( ns.getCharacterInformation().city != "Volhaven"){
                ns.travelToCity("Volhaven");
            }
            if(ns.universityCourse('ZB Institute of Technology','Leadership')){
                await ns.sleep(statSleep);
            }
        }
        if( !hasToured && ns.getServerMoneyAvailable("home") > 55000000) {
            ns.print("Going on a world tour!");
            idle(ns); //avoids staying in a training course during the tour
            for(let i=0;i<cities.length;i++){
                ns.travelToCity(cities[i]);
                await ns.sleep(60000);
            }
            hasToured = true;
            ns.write(tourTime,ns.getTimeSinceLastAug(),"w");
            ns.print("Let's make a note so we don't forget this trip of a lifetime!");
        }
        if(myCity != ns.getCharacterInformation().city){
            ns.print("Going back home");
            ns.travelToCity(myCity);
        }
        
        if(!hasTor && ns.getServerMoneyAvailable("home") > 1000000) {
            ns.purchaseTor();
            hasTor = ns.getCharacterInformation().tor;
        }
        if(hasTor && numPrograms < 5) {
            if (ns.fileExists("BruteSSH.exe", "home")) {
                numPrograms = 1;
                if (ns.fileExists("FTPCrack.exe", "home")) {
                    numPrograms++;
                    if (ns.fileExists("relaySMTP.exe", "home")) {
                        numPrograms++;
                        if (ns.fileExists("HTTPWorm.exe", "home")) {
                            numPrograms++;
                            if (ns.fileExists("SQLInject.exe", "home")) {
                                numPrograms++;
                            } else {
                                ns.purchaseProgram("SQLInject.exe");
                            }
                        } else {
                            ns.purchaseProgram("HTTPWorm.exe");
                        }
                    } else {
                        ns.purchaseProgram("relaySMTP.exe");
                    }
                } else {
                    ns.purchaseProgram("FTPCrack.exe");
                }
            } else {
                ns.purchaseProgram("BruteSSH.exe");
            }
        }
        
        //Commit crimes if low on money
        if(ns.getServerMoneyAvailable("home") < moneyTarget) {
            await tryCrime(ns, moneyTarget);
        }
        if(hasTor && !ns.fileExists("DeepscanV1.exe", "home")) {
            ns.purchaseProgram("DeepscanV1.exe");
        }
        if(hasTor && !ns.fileExists("DeepscanV2.exe", "home")) {
            ns.purchaseProgram("DeepscanV2.exe");
        }
        if(hasTor && !ns.fileExists("AutoLink.exe", "home")) {
            ns.purchaseProgram("AutoLink.exe");
        }
        if(hasTor && !ns.fileExists("ServerProfiler.exe", "home")) {
            ns.purchaseProgram("ServerProfiler.exe");
        }
        
        if(ns.getServerMoneyAvailable("home") > ns.getUpgradeHomeRamCost()) {
            ns.print("Buying more RAM for my home computer");
            ns.upgradeHomeRam();
        }
        
        await ns.sleep(100);
    }
    
}
