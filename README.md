# bitburner-scripts

Scripts for use with the [Bitburner](https://danielyxie.github.io/bitburner/#) game. Written in [Netscript](http://bitburner.wikia.com/wiki/Netscript), which seems to be a customized subset of javascript.
